from django.db import models
from users.models import Person

class Laptop(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, default=1, null=True, blank=True)
    name = models.CharField(max_length=50)
    brand = models.CharField(max_length=100)
    description = models.TextField(max_length=1000)
    price = models.FloatField(default=100)
    is_sale = models.BooleanField(default=False)
    percent_sale = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '{} {} {}$'.format(self.name, self.brand, self.price)

    def get_price(self):
        if self.is_sale:
            return self.price * (1 - self.percent_sale / 100)
        else:
            return self.price