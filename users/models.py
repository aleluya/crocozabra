from django.db import models
from django.contrib.auth.models import User

# Create your models here.
MAX_RATE = 5


class Person(models.Model):
    STATUS_CHOICES = (
        ('golden', 'Золотой продавец'),
        ('silver', 'Серебрянный продавец'),
        ('simple', 'Продавец'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    phone_number = models.CharField(max_length=20, blank=True, null=True)
    date_birth = models.DateField(blank=True, null=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=20)
    rate = models.FloatField(default=4, blank=True, null=True)
    count_sold_products = models.IntegerField(default=0)
    online = models.BooleanField(default=False)

    # TODO: add type of products

    def __str__(self):
        return '{} {} {}'.format(self.user.first_name, self.user.last_name, self.user.email)

    def get_name(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)

    def get_rate(self):
        return (self.rate / MAX_RATE) * 100

   # def get_phones(self):
       # return Phone.objects.filter(person=self)

